var telegram = require('telegram-bot-api');
var http = require('http');
var sqlite3 = require('sqlite3').verbose();

var db = new sqlite3.Database('storage.db');

db.run('CREATE TABLE IF NOT EXISTS [messages] ([tid] INTEGER(11) NOT NULL, [uid] INTEGER(11) NOT NULL, [ctime] DATETIME NOT NULL DEFAULT current_timestamp, [message] TEXT(8192) DEFAULT NULL, [json] TINYINT(1) NOT NULL DEFAULT 0); CREATE UNIQUE INDEX [messages_tid] ON [messages] ([tid]);');
db.run('CREATE TABLE IF NOT EXISTS [users] ([tid] INTEGER(11) DEFAULT NULL, [first_name] CHAR(64) DEFAULT NULL, [last_name] CHAR(64) DEFAULT NULL, [username] CHAR(64) DEFAULT NULL, [ctime] DATETIME NOT NULL DEFAULT current_timestamp);CREATE UNIQUE INDEX [unique_tid] ON [users] ([tid]);');

var api = new telegram({
	token: '154114991:AAH_LSv_7PzRvpb8v3CPAy3db9v6uxeqikk',
	updates: {
		enabled: true,
		pooling_timeout: 60000,
		get_interval: 500
	}
});

api.on('message', function(message) {
	console.log(message);
	var chat_id = message.chat.id;
	if(message.text == '/start') {
		show_start_menu(message);
	}
	else if(message.text == 'Date') {
		var date = new Date();
		date = date.toString();
		api.sendMessage({chat_id: chat_id, text : date });
	}
	else if(message.text == 'Joke') {
		tell_joke(message);
	}
	else if(message.text == 'Exit') {
		hide_menu(message);
	}
	else if(message.hasOwnProperty('sticker') || message.hasOwnProperty('photo')) {
		api.sendMessage({chat_id: chat_id, text: 'Nice pic!'});
	}
	else {
		api.sendMessage({chat_id: chat_id, text : 'You say "'+ message.text +'", but it\'s absolutely pointless'});
	}
	store_message(message);
});

function store_message(message) {
	add_user(message.from);
	var sql = 'insert or ignore into messages (tid, uid, message, json) values($tid, $uid, $message, $json)';
	var message_text = null, is_json = false;
	if(message.hasOwnProperty('text')) {
		message_text = message.text || null;
	}
	else if(message.hasOwnProperty('sticker')) {
		message_text = JSON.stringify(message.sticker);
		is_json = true;
	}
	else if(message.hasOwnProperty('photo')) {
		message_text = JSON.stringify(message.photo);
		is_json = true;
	}
	var params = {
		$tid: message.message_id,
		$uid: message.from.id,
		$message: message_text,
		$json: is_json
	};
	db.run(sql, params);
}

function add_user(user) {
	console.log(user);
	var sql = 'insert or ignore into users (tid, first_name, last_name, username) values($tid, $first_name, $last_name, $username)';
	var params = {
		$tid: user.id,
		$first_name: user.first_name || null,
		$last_name: user.last_name || null,
		$username: user.username || null
	};
	db.run(sql, params);
}

function show_start_menu(message) {
	var menu = {
		keyboard: [
		    ['Joke', 'Date'],
		    ['Exit']
		],
		resize_keyboard: true,
		selective: false
	};
	menu = JSON.stringify(menu);
	api.sendMessage({
		chat_id: message.chat.id,
		text: 'Choose action:',
		reply_markup: menu
	});
}

function hide_menu(message) {
	var menu = {
		hide_keyboard: true
	};
	menu = JSON.stringify(menu);
	api.sendMessage({
		chat_id: message.chat.id,
		text: 'Say /start to show menu',
		reply_markup: menu
	});
}

function tell_joke(message) {
	http.get({
		hostname: 'qrcode.tk',
		port: 80,
		path: '/ajax/tell_me_a_joke'
	}, function (res) {
		var body = '';
		res.on('data', function (chunk) {
			body += chunk;
		});
		res.on('end', function() {
			var data = JSON.parse(body);
			if(data.error) {
				api.sendMessage({chat_id: message.chat.id, text: 'Error! ' + data.error});
			}
			else {
				// console.log(data);
				var joke = data.joke.content;
				joke = joke.replace('\n\n Source: kickasshumor.com\n', '');
				api.sendMessage({chat_id: message.chat.id, text: joke});
			}
		});
	});
}
