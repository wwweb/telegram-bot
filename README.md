# README #

This is a simple telegram bot example. I made it just to get started with Telegram bot API.

# Installation #

* clone repository
* replace bot token with yours
* 'npm install' in project directory
* run the app with 'node bot.js'

Requires sqlite3 module, which will probably fail to install. Just install/update gcc compiler and retry 'npm install'. In Windows you need installed MS Visual Studio and it's C++ compiler (or it was C#? - not sure).